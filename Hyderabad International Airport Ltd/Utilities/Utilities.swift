//
//  Utilities.swift
//  Hyderabad International Airport Ltd
//
//  Created by ceazeles on 02/05/19.
//  Copyright © 2019 ceazeles. All rights reserved.
//

import Foundation
import UIKit

class Utilities: NSObject {
    static let sharedInstance = Utilities()
    
    func displayAlert(title:String,message:String,forController:UIViewController,completion: @escaping (_ callBack:Bool) -> ()) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
            completion(true)
        }))
        
        alert.view.tintColor = UIColor.init(red: 225.0/255.0, green: 49.0/255.0, blue: 91/255.0, alpha: 1.0)
        forController.present(alert, animated: true)
    }

}
