//
//  UIImage.swift
//  Hyderabad International Airport Ltd
//
//  Created by ceazeles on 02/05/19.
//  Copyright © 2019 ceazeles. All rights reserved.
//

import Foundation
import UIKit

extension UIImage {
    convenience init?(url: URL?) {
        guard let url = url else { return nil }
        
        do {
            let data = try Data(contentsOf: url)
            self.init(data: data)
        } catch {
            print("Cannot load image from url: \(url) with error: \(error)")
            return nil
        }
    }
}
