//
//  DashboardViewController.swift
//  Hyderabad International Airport Ltd
//
//  Created by ceazeles on 02/05/19.
//  Copyright © 2019 ceazeles. All rights reserved.
//

import UIKit
import Alamofire
import Kingfisher

class DashboardViewController: UIViewController,UIGestureRecognizerDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    var imageList = [Dashboard]()
    var indexList = [String]()
    
    var rowIndex : Int = 0
    var rowValue : Int = 0
    
    var noDataLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.layer.masksToBounds = true
        label.layer.shadowOpacity = 2.5
        label.layer.shadowColor = UIColor.gray.cgColor
        label.layer.shadowOffset = CGSize(width: 0, height:1)
        label.layer.shadowRadius = 2
        label.text = "No Data Found"
        label.font = UIFont(name: "System", size: 22)
        return label
    }()
    
    var timer = Timer()
    var isTimerRunning = false //This will be used to make sure only one timer is created at a time.
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.noDataLabel.frame =  CGRect(x: 0, y: self.view.frame.size.height/2, width: self.view.frame.size.width, height: 50)
        self.getImageList(url: "GetImages")
        self.runTimer()
        let lpgr = UILongPressGestureRecognizer(target: self, action: #selector(handleLongPress))
        lpgr.minimumPressDuration = 0.5
        lpgr.delaysTouchesBegan = true
        lpgr.delegate = self
        self.tableView.addGestureRecognizer(lpgr)
    }
    
    func runTimer() {
        timer = Timer.scheduledTimer(timeInterval: 20, target: self,   selector: (#selector(self.updateTimer)), userInfo: nil, repeats: true)
    }
    
    @objc func updateTimer() {
        if(rowIndex < imageList.count-1)
        {
            rowIndex += 1
        }
        else{
            rowIndex = 0
        }
        
        self.tableView.reloadData()
    }
    
    //MARK: - UILongPressGestureRecognizer Action -
    @objc func handleLongPress(gestureReconizer: UILongPressGestureRecognizer) {
        if gestureReconizer.state != UIGestureRecognizer.State.ended {
            //When lognpress is start or running
            timer.invalidate()
        }
        else {
            //When lognpress is finish
            runTimer()
        }
    }
    
    
    @IBAction func nextButtonTapped(_ sender: Any) {
        self.updateTimer()
    }
    
    @IBAction func previousButtonTapped(_ sender: Any) {
        if(rowIndex > 0)
        {
            rowIndex -= 1
        }
        else{
            rowIndex = imageList.count-1
        }
        
        self.tableView.reloadData()
    }
    
    
    func getImageList(url : String) {
        if NetworkReachabilityManager()!.isReachable {
            
            Alamofire.request(Constants.BASE_URL + url , method: .get, parameters: nil, encoding: JSONEncoding.default).responseJSON {
                response in
                if response.result.isSuccess{
                    DispatchQueue.main.async{
                        
                        print(response)
                        
                        guard let data = response.data else { return }
                        do {
                            let decoder = JSONDecoder()
                            self.imageList = try decoder.decode(Array<Dashboard>.self, from: data)
                            
                            
                        } catch let error {
                            print(error)
                        }
                        
                        if self.imageList.count > 0
                        {
                            self.hideNoData()
                        }
                        else
                        {
                            self.showNoData()
                        }
                    }
                }
                else
                {
                    print("Error \(String(describing: response.result.error))")
                }
            }
        }
        else{
            Utilities.sharedInstance.displayAlert(title: "", message: "Please check the internet connectivity and try again.", forController: self, completion: {_ in })
        }
    }
    
    func showNoData() {
        tableView.isHidden = true
        if !noDataLabel.isDescendant(of: self.view)
        {
            self.view.addSubview(noDataLabel)
        }
    }
    
    func hideNoData() {
        tableView.isHidden = false
        self.tableView.reloadData()
        noDataLabel.removeFromSuperview()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        timer.invalidate()
    }
    
    
}

extension DashboardViewController : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if(indexPath.section == 0)
        {
            return 350
        }
        else
        {
            return UITableView.automaticDimension
        }
    }
}


extension DashboardViewController : UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(imageList.count > 0)
        {
            if(section == 0 || section == 1)
            {
                return 1
            }
            else if section == 2{
                let item = imageList[rowIndex]
                rowValue = item.lstActivityDesignRevision.count
                return item.lstActivityDesignRevision.count
            }
            else if section == 3
            {
                let item = imageList[rowIndex]
                return item.lstActivityDesignMilestone.count
            }
            else
            {
               let item = imageList[rowIndex]
                if(item.description == "" || item.description == "NA")
                {
                    return 0
                }
                else{
                    return 1
                }
                
            }
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(indexPath.section == 0)
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "dashboardCell", for: indexPath) as! DashboardTableViewCell
            
            let item = imageList[rowIndex]
            let urlString = item.filePath.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
            let url = URL(string:urlString ?? "")
            cell.dashboardImage.kf.setImage(with: url)
            cell.titleLabel.text = item.activityName
            return cell
        }
        else if(indexPath.section == 1)
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "countCell", for: indexPath) as! DashboardTableViewCell
            
            let item = imageList[rowIndex]
            cell.yearLabel.text = "\(item.year)"
            cell.monthLabel.text = "\(item.month)"
            cell.dayLabel.text = "\(item.day)"
            cell.daysLeftLabel.text = "\(item.daysLeft)"
            
            return cell
        }
        else  if(indexPath.section == 2)
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "revisionCell", for: indexPath) as! DashboardTableViewCell
            
            let revision = imageList[rowIndex].lstActivityDesignRevision[indexPath.row]
            
            cell.revisionLabel.text = "\(rowValue)"
            cell.revisionYearLabel.text = revision.year
            cell.revisionMonthLabel.text = revision.month
            cell.revisionDaysLabel.text = revision.day
            cell.revisionDaysLaftLabel.text = "\(revision.daysLeft ?? 00)"
            rowValue = rowValue - 1
            return cell
        }
        else if(indexPath.section == 3)
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "milestoneCell", for: indexPath) as! DashboardTableViewCell
            
            let milestone = imageList[rowIndex].lstActivityDesignMilestone.reversed()[indexPath.row]
            cell.milestoneDescLabel.text = milestone.milestoneName
            cell.milestoneYearLabel.text = milestone.year
            cell.milestoneMonthLabel.text = milestone.month
            cell.milestoneDaysLabel.text = milestone.day
            cell.milestoneDaysLaftLabel.text = "\(milestone.daysLeft ?? 00)"
            
            return cell
        }
        else{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "remarksCell", for: indexPath) as! DashboardTableViewCell
            let item = imageList[rowIndex]
            cell.remarksTextView.text = item.description
            
            return cell
            
        }
    }
}
