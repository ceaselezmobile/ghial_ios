//
//  DashboardModel.swift
//  Hyderabad International Airport Ltd
//
//  Created by ceazeles on 02/05/19.
//  Copyright © 2019 ceazeles. All rights reserved.
//

import Foundation


struct Dashboard: Codable {
    let activityDesignID, activityEndDate, activityMilestoneID, activityName: String
    let createdBy, createdByName, createdDate, day: String
    let daysLeft: Int
    let description: String
    let filePath: String
    let isMilestoneAdded, month, projectID, shortDescription: String
    let year: String
    let lstActivityDesignMilestone: [LstActivityDesignMilestone]
    let lstActivityDesignRevision: [LstActivityDesignRevision]
    
    enum CodingKeys: String, CodingKey {
        case activityDesignID = "ActivityDesignID"
        case activityEndDate = "ActivityEndDate"
        case activityMilestoneID = "ActivityMilestoneID"
        case activityName = "ActivityName"
        case createdBy = "CreatedBy"
        case createdByName = "CreatedByName"
        case createdDate = "CreatedDate"
        case day = "Day"
        case daysLeft = "DaysLeft"
        case description = "Description"
        case filePath = "FilePath"
        case isMilestoneAdded = "IsMilestoneAdded"
        case month = "Month"
        case projectID = "ProjectID"
        case shortDescription = "ShortDescription"
        case year = "Year"
        case lstActivityDesignMilestone, lstActivityDesignRevision
    }
}

struct LstActivityDesignMilestone: Codable {
    let activityEndDate, activityMilestoneID, createdBy, createdDate: String?
    let day: String?
    let daysLeft: Int?
    let description, milestoneID, milestoneName, month: String?
    let sNo: Int?
    let year: String?
    
    enum CodingKeys: String, CodingKey {
        case activityEndDate = "ActivityEndDate"
        case activityMilestoneID = "ActivityMilestoneID"
        case createdBy = "CreatedBy"
        case createdDate = "CreatedDate"
        case day = "Day"
        case daysLeft = "DaysLeft"
        case description = "Description"
        case milestoneID = "MilestoneID"
        case milestoneName = "MilestoneName"
        case month = "Month"
        case sNo = "SNo"
        case year = "Year"
    }
}

struct LstActivityDesignRevision: Codable {
    let activityDesignID, activityDesignRevisionID, activityMilestoneID, activityRevisedEndDate: String?
    let createdBy, createdDate, day: String?
    let daysLeft: Int?
    let month, year: String?
    let lstActivityDesignMilestone: String?
    
    enum CodingKeys: String, CodingKey {
        case activityDesignID = "ActivityDesignID"
        case activityDesignRevisionID = "ActivityDesignRevisionID"
        case activityMilestoneID = "ActivityMilestoneID"
        case activityRevisedEndDate = "ActivityRevisedEndDate"
        case createdBy = "CreatedBy"
        case createdDate = "CreatedDate"
        case day = "Day"
        case daysLeft = "DaysLeft"
        case month = "Month"
        case year = "Year"
        case lstActivityDesignMilestone = "lstActivityDesignMilestone"
    }
}
