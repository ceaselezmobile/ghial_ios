//
//  DashboardTableViewCell.swift
//  Hyderabad International Airport Ltd
//
//  Created by ceazeles on 02/05/19.
//  Copyright © 2019 ceazeles. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

class DashboardTableViewCell: UITableViewCell {

    @IBOutlet weak var dashboardImage: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var yearLabel: UILabel!
    @IBOutlet weak var monthLabel: UILabel!
    @IBOutlet weak var dayLabel: UILabel!
    @IBOutlet weak var daysLeftLabel: UILabel!
    
    //DesignRevision
    @IBOutlet weak var revisionLabel: UILabel!
    @IBOutlet weak var revisionYearLabel: UILabel!
    @IBOutlet weak var revisionMonthLabel: UILabel!
    @IBOutlet weak var revisionDaysLabel: UILabel!
    @IBOutlet weak var revisionDaysLaftLabel: UILabel!
    
    
    //MilestoneDesign
    @IBOutlet weak var milestoneDescLabel: UILabel!
    @IBOutlet weak var milestoneYearLabel: UILabel!
    @IBOutlet weak var milestoneMonthLabel: UILabel!
    @IBOutlet weak var milestoneDaysLabel: UILabel!
    @IBOutlet weak var milestoneDaysLaftLabel: UILabel!
    
    //Remarks
    
    @IBOutlet weak var remarksTextView: IQTextView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
